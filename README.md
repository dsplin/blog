# Creating a blog with Laravel

## Requirements

Before starting the deployment, make sure your system meets the following requirements:

- Node.js
- Docker
- Docker Compose

## Deployment

To deploy the project, follow these steps:

1. Clone the repository:
   ```shell
   git clone git@gitlab.com:dsplin/blog.git
   
2. Navigate to the project directory:
   ```shell
   cd blog
   
3. Start the Docker containers:
   ```shell
   docker-compose up -d

4. Go to the back (laravel) container:
   ```shell
   docker-compose exec back bash
   
5. Run these commands
   ```shell
   composer install
   
   npm install 
   (if you get an error, delete the node_modules directory and try again)
   
   npx mix
   
   php artisan migrate
   
   php artisan db:seed
   
6. Run tests
   ```shell
   php artisan test
   
7. Once the containers are successfully running, you can access your application using the following URL:
   ```shell
   http://localhost:8000

