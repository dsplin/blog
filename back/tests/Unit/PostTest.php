<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\Post;

class PostTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test creating a new post.
     *
     * @return void
     */
    public function testCreatePost()
    {
        $post = Post::factory()->create([
            'title' => 'Test Post',
            'content' => 'This is a test post.',
        ]);

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'title' => 'Test Post',
            'content' => 'This is a test post.',
        ]);
    }

    /**
     * Test updating a post.
     *
     * @return void
     */
    public function testUpdatePost()
    {
        $post = Post::factory()->create();

        $post->title = 'Updated Post';
        $post->save();

        $this->assertEquals('Updated Post', $post->title);
    }

    /**
     * Test deleting a post.
     *
     * @return void
     */
    public function testDeletePost()
    {
        $post = Post::factory()->create();

        $post->delete();

        $this->assertDatabaseMissing('posts', ['id' => $post->id]);
    }
}
