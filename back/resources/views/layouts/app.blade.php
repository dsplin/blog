<!DOCTYPE html>
<html>
<head>
    <title>My Blog</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<header>
    <div class="container">
        <h1>My Blog</h1>
    </div>
</header>

<div class="content">
    @yield('content')
</div>

<footer>
    <div class="container">
        <p>© 2023 My Blog. All rights reserved.</p>
    </div>
</footer>

<!--<script src="{{ asset('js/scripts.js') }}"></script>-->
</body>
</html>
