<div class="row justify-content-center mt-4">
    <nav aria-label="Posts Pagination">
        <ul class="pagination justify-content-center">
            @if ($posts->onFirstPage())
            <li class="page-item disabled">
                <span class="page-link">Previous</span>
            </li>
            @else
            <li class="page-item">
                <a class="page-link" href="{{ $posts->previousPageUrl() }}" rel="prev">Previous</a>
            </li>
            @endif

            @for ($page = 1; $page <= $totalPages; $page++)
            <li class="page-item {{ $page == $posts->currentPage() ? 'active' : '' }}">
                <a class="page-link" href="{{ $posts->url($page) }}">{{ $page }}</a>
            </li>
            @endfor

            @if ($posts->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $posts->nextPageUrl() }}" rel="next">Next</a>
            </li>
            @else
            <li class="page-item disabled">
                <span class="page-link">Next</span>
            </li>
            @endif
        </ul>
    </nav>
</div>
